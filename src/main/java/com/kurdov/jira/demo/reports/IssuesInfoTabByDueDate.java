package com.kurdov.jira.demo.reports;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.datetime.DateTimeFormatter;
import com.atlassian.jira.datetime.DateTimeFormatterFactory;
import com.atlassian.jira.datetime.DateTimeStyle;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.search.SearchException;
import com.atlassian.jira.issue.search.SearchProvider;
import com.atlassian.jira.issue.search.SearchResults;
import com.atlassian.jira.jql.builder.JqlClauseBuilder;
import com.atlassian.jira.jql.builder.JqlQueryBuilder;
import com.atlassian.jira.plugin.report.impl.AbstractReport;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.util.ParameterUtils;
import com.atlassian.jira.web.action.ProjectActionSupport;
import com.atlassian.jira.web.bean.PagerFilter;
import com.atlassian.plugin.spring.scanner.annotation.component.Scanned;
import com.atlassian.plugin.spring.scanner.annotation.imports.JiraImport;
import com.atlassian.query.Query;
import org.apache.log4j.Logger;

import java.sql.Timestamp;
import java.util.*;

@Scanned
public class IssuesInfoTabByDueDate extends AbstractReport {

    private static final Logger log = Logger.getLogger(IssuesInfoTabByDueDate.class);

    private JiraAuthenticationContext jiraAuthCtx = ComponentAccessor.getJiraAuthenticationContext();
    private final DateTimeFormatter formatter;
    @JiraImport
    private final ProjectManager projectManager;
    @JiraImport
    private final SearchProvider searchProvider;

    private List<Issue> issuesBeforeDueDate;
    private Date selectedDueDate;

    public IssuesInfoTabByDueDate(@JiraImport DateTimeFormatterFactory dateTimeFormatterFactory,
                                  ProjectManager projectManager,
                                  SearchProvider searchProvider) {
        this.formatter = dateTimeFormatterFactory.formatter().withStyle(DateTimeStyle.DATE).forLoggedInUser();
        this.projectManager = projectManager;
        this.searchProvider = searchProvider;
    }

    public String generateReportHtml(ProjectActionSupport projectActionSupport, Map params) throws Exception {

        List<Project> projects = projectManager.getProjects();
        getIssuesBeforeDueDate(getAllIssues(projects), selectedDueDate);

        Map<String, Object> velocityParams = new HashMap<String, Object>();
        velocityParams.put("selectedDueDate", formatter.format(selectedDueDate));
        velocityParams.put("issues", issuesBeforeDueDate);
        velocityParams.put("formatter", formatter);

        return descriptor.getHtml("view", velocityParams);
    }

    private void getIssuesBeforeDueDate(List<Issue> allIssues, Date selectedDueDate) {

        issuesBeforeDueDate = new ArrayList<Issue>();

        Timestamp selectedDueDateTimeStamp = new Timestamp(selectedDueDate.getTime());
        for (Issue issue : allIssues) {
            if (issue.getDueDate() != null && issue.getDueDate().before(selectedDueDateTimeStamp)) {
                issuesBeforeDueDate.add(issue);
            }
        }
    }

    private List<Issue> getAllIssues(List<Project> projects) throws SearchException {

        List<Issue> allIssues = new ArrayList<Issue>();

        for (Project specifiedProject : projects) {

            if (specifiedProject.getProjectLead().equals(jiraAuthCtx.getLoggedInUser())) { //remove, if need to inspect over all projects

                JqlClauseBuilder builderAllIssues = JqlQueryBuilder.newClauseBuilder();
                JqlClauseBuilder tempQueryAllIssues = builderAllIssues.project(specifiedProject.getId());

                Query queryAllIssues = tempQueryAllIssues.buildQuery();
                SearchResults searchResultsAllIssues =
                        searchProvider.search(queryAllIssues, specifiedProject.getProjectLead(), PagerFilter.getUnlimitedFilter());

                allIssues.addAll(searchResultsAllIssues.getIssues());
            }
        }

        return allIssues;
    }

    @Override
    public boolean showReport() {
        return (ComponentAccessor.getCrowdService().isUserMemberOfGroup(jiraAuthCtx.getLoggedInUser().getName(), "jira-administrators"));
    }

    @Override
    public void validate(ProjectActionSupport action, Map params) {

        String dueDateParam = ParameterUtils.getStringParam(params, "dueDate");

        if (dueDateParam.isEmpty()) {
            selectedDueDate = new Date();
            return;
        }

        try {
            selectedDueDate = formatter.parse(dueDateParam);
        } catch (IllegalArgumentException e) {
            action.addError("selectedDueDate", action.getText("report.infotab.selectedduedate.required"));
            log.error("Exception while parsing dueDate");
        }
    }
}
